--
--
-- Использованная кодировка текста: windows-1251
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: spec_doctor
CREATE TABLE spec_doctor (id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, name_spec STRING (100));
INSERT INTO spec_doctor (id, name_spec) VALUES (1, 'Офтальмолог');
INSERT INTO spec_doctor (id, name_spec) VALUES (2, 'Невролог');
INSERT INTO spec_doctor (id, name_spec) VALUES (3, 'Хирург');
INSERT INTO spec_doctor (id, name_spec) VALUES (4, 'Терапевт');

-- Таблица: department
CREATE TABLE department (id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, department STRING (100) UNIQUE);
INSERT INTO department (id, department) VALUES (10, 'Офтальмологическое');
INSERT INTO department (id, department) VALUES (11, 'Неврологическое');
INSERT INTO department (id, department) VALUES (12, 'Терапевтическое 1');
INSERT INTO department (id, department) VALUES (13, 'Хирургическое 1');

-- Таблица: doctor
CREATE TABLE doctor (id INTEGER PRIMARY KEY AUTOINCREMENT, name_doctor STRING (150), id_office INTEGER, id_spec INTEGER, department_id INTEGER);
INSERT INTO doctor (id, name_doctor, id_office, id_spec, department_id) VALUES (1, 'Иванов ВВ', 1, 1, 10);
INSERT INTO doctor (id, name_doctor, id_office, id_spec, department_id) VALUES (2, 'Петров АА', 2, 3, 11);
INSERT INTO doctor (id, name_doctor, id_office, id_spec, department_id) VALUES (3, 'Сидоров ПП', 3, 2, 12);

-- Таблица: reception
CREATE TABLE reception (date_write DATETIME PRIMARY KEY DEFAULT (datetime()), pacient STRING (200) NOT NULL, doctor_id INTEGER, office_id INTEGER, date_reception DATETIME DEFAULT (datetime()), birthday DATE);
INSERT INTO reception (date_write, pacient, doctor_id, office_id, date_reception, birthday) VALUES ('2021-10-13 14:26:24', 'Иванова Павел Алексеевич', 1, 2, '2021-10-13 14:27:38', '1980-09-02');
INSERT INTO reception (date_write, pacient, doctor_id, office_id, date_reception, birthday) VALUES ('2021-10-13 14:36:43', 'Карпов Илья Васильевич', 2, 3, '', '1978-05-10');
INSERT INTO reception (date_write, pacient, doctor_id, office_id, date_reception, birthday) VALUES ('2021-10-13 23:20:05', 'Тихонов Иван Вадимович', 2, 3, '2021-10-13 23:20:05', '1984-09-08');

-- Таблица: office
CREATE TABLE office (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, name_office STRING (20));
INSERT INTO office (id, name_office) VALUES (1, 102);
INSERT INTO office (id, name_office) VALUES (2, 110);
INSERT INTO office (id, name_office) VALUES (3, 105);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
