import json, sqlite3
path_db = 'E:/sqlitestudio/medical_reception.db'

def get_data(sqltext='select 1'):
    con = sqlite3.connect(path_db)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    
    rows = cur.execute(sqltext).fetchall()
    
    cur.close()
    con.close()	
    
    return json.dumps([dict(key_val) for key_val in rows], ensure_ascii=False)