from bottle import auth_basic, request, route, debug, run, error, response, template, redirect
import model

def is_authenticated_user(user, password):
    if user in 'admin' and password == 'kichAE56cvbf474':
        return True
    else:
        return False 

@error(401)
@error(404)
@error(403)
@error(500)
def mistake(code):
    return json.dumps(dict(error = response.body, status_code = response.status_code))

url = '/public'

@route('/')
@route(url)
@auth_basic(is_authenticated_user)
def home():
    return '<h2>здесь будет документация  по API ...</h2>'
    #redirect('docs.htm')

@route(f"{url}/get/doctor")
@auth_basic(is_authenticated_user)
def get_doctor():
    query = 'select d.name_doctor, o.name_office, s.name_spec, dp.department  from  doctor as d inner join   office as o on o.id = d.id_office inner join   spec_doctor as s   on s.id = d.id_spec inner join   department as dp   on dp.id = d.department_id order by  d.name_doctor, s.name_spec, dp.department'
    return model.get_data(query)

@route(f"{url}/get/spec")
@auth_basic(is_authenticated_user)
def get_spec():
    query = 'select s.id, s.name_spec from spec_doctor as sorder by s.name_spec'
    return model.get_data(query)

@route(f"{url}/get/department")
@auth_basic(is_authenticated_user)
def get_department():
    query = 'select d.id, d.department from department as d order by d.department'
    return model.get_data(query)

@route(f"{url}/get/reception")
@auth_basic(is_authenticated_user)
def get_reception():
    doc_id = request.query.doctor or '0'
    query = 'select  r.pacient, r.birthday, r.date_reception, o.name_office from  reception as r inner join  doctor as d on d.id = r.doctor_id  inner join office as o  on o.id = r.office_id  where d.id = {}'.format(doc_id) 
    return  model.get_data(query)


@route(f"{url}/get/pacient")
@auth_basic(is_authenticated_user)

def get_pacient():
    birthday = request.query.birthday or '0000-00-00'
    query = '''
	  select 
		r.date_reception,
		r.pacient,
		r.birthday,
		d.name_doctor,
		o.name_office
	  from 
		  reception as r
	  inner join 
		  office as o
		on o.id = r.office_id
	  inner join 
		  doctor as d
		on d.id = r.doctor_id
		where  
		  r.birthday like '{}'
		order by r.date_reception
	'''.format(birthday)
    
    return model.get_data(query)
# ... здесь будут описаны остальныке методы для adm

if __name__ == '__main__': run(reloader=True)